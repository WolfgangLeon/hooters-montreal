<?php 

	/*
	**			 
	**             French page
	**
	*/
	
	$ctrl = new App();

?>

<?php $ctrl->getBlock('head') ?>

<body>

<div id="wrapper" class="wrapper-flexi">

	<?php $ctrl->getBlock('header') ?>

	<!-- Start Content -->

		<div class="container">
		
            <div class="row"> 
	        	
	        	<div class="span3">&nbsp;</div>
	        	
	        	<div class="span6">
	        	
	        		<div class="inner">
	        		
	        			<div class="form-box">
	        			
	        				<div class="top">

		        				<!--Please complete the following form to ensure your reservation.-->

		        				<div class="inner intro align-center">
					        		<h1>Hooters Montreal</h1>

					        		<h2>Centre-ville emplacement Ouvert! </h2>
					        		<p>Notre site web est présentement en construction.<br>Pendant ce temps venez nous visitez!</p>		        				
					        	</div> 
		        				
	        				</div>
	        			
	        				<div class="bottom"> 

								<div  id="success" style="display:none" >
								    <span class="green textcenter">
								        <p class="bg-success alert">Nous vous remercions de votre réservation. Veuillez vérifier votre email pour une confirmation. À moins qu'il y ait un problème de vérification pour un grand groupe, ou nous ne pouvons pas réserver des places, il n'est pas nécessaire que nous vous appellions. Si vous n'avez pas reçu de confirmation, vérifiez votre dossier de courrier indésirable.</p>
								    </span>
								</div>

								<div id="error" style="display:none" >
								    <span>
								        <p class="bg-alert alert">Quelque chose a mal tourné. Veuillez actualiser et réessayer.</p>
								    </span>
								</div>
																	        				
								<form id="signup" name="signup" onsubmit="return false">
									
									<input type="hidden" name="lang" id="lang" value="<?php echo $ctrl->_ln ?>">
									
									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Votre nom * 
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="text" name="name" id="name" size="30" value="" required="" class="text login_input"  placeholder="Votre nom">
										</div>
										<div class="clearfix"></div>
									</div>  

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Téléphone *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="number" name="phone" id="phone" size="30" value="" required="" class="text login_input"  placeholder="Numéro de téléphone">
										</div>
										<div class="clearfix"></div>
									</div>
									
									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Courriel *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="text" name="email" id="email" size="30" value="" required="" class="text login_input"  placeholder="Address courriel">
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Invités *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="number" name="guest" id="guest" size="30" value="" required="" class="text login_input"  placeholder="Combien de personnes?">
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Occassion *
												</label>
											</div>
										</div>
										<div class="span7">
											<select name="matter" id="matter" required="" class="text login_input">
												<option value="Sporting Event">Evénement sportif</option>
												<option value="Private Party">Soirée privée</option>
												<option value="Dining">Souper</option>
												<option value="Bar">Bar</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Heure *
												</label>
											</div>
										</div>
										<div class="span7">
											<select name="time" id="time" required="" class="text login_input">
												<option value="12:00 pm">12:00 pm</option>
												<option value="12:30 pm">12:30 pm</option>
												<option value="1:00 pm">1:00 pm</option>
												<option value="1:30 pm">1:30 pm</option>
												<option value="2:00 pm">2:00 pm</option>
												<option value="2:30 pm">2:30 pm</option>
												<option value="3:00 pm">3:00 pm</option>
												<option value="3:30 pm">3:30 pm</option>
												<option value="4:00 pm">4:00 pm</option>
												<option value="4:30 pm">4:30 pm</option>
												<option value="5:00 pm">5:00 pm</option>
												<option value="5:30 pm">5:30 pm</option>
												<option value="6:00 pm">6:00 pm</option>
												<option value="6:30 pm">6:30 pm</option>
												<option value="7:00 pm">7:00 pm</option>
												<option value="7:30 pm">7:30 pm</option>
												<option value="8:00 pm">8:00 pm</option>
												<option value="8:30 pm">8:30 pm</option>
												<option value="9:00 pm">9:00 pm</option>
												<option value="9:30 pm">9:30 pm</option>
												<option value="10:00 pm">10:00 pm</option>
												<option value="10:30 pm">10:30 pm</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Date *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="text" name="date" id="date" size="30" value="" required="" autocomplete="off" class="text login_input date calendar"  placeholder="Choisissez la date">
										</div>
										<div class="clearfix"></div>
									</div>  

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Emplacement  *
												</label>
											</div>
										</div>
										<div class="span7">
											<select name="location" id="location" required="" class="text login_input">
												<option value="1433 Rue Crescent, Montreal">Centre-ville</option>
												<option value="3320 Taschereau Blvd, Greenfield Park">Rive-Sud</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									
									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Demande spéciale
												</label>
											</div>
										</div>
										<div class="span7">
											<textarea name="description" id="description" placeholder="Proche d’une fenêtre, étage, assis au bar, anniversaire, etc."></textarea>
										</div>
										<div class="clearfix"></div>
									</div>  
									    								
									<div class="form-row">					 
										<input id="submit" type="submit" name="submit" value="Faire une réservation" class="btn btn-wide btn-extrawide" data-loading-text="Loading...">
										<div class="clearfix"></div>
									</div>   
									
								</form>
								
							</div> 
	        			
	        			</div>
	        			<div class="shadow"></div>
	        			<div class="clearfix"></div>
	        			
	        			
	        		</div>
	        		
	        	</div>
	        	
            </div>  
		
		</div>
	
	<!-- End content -->	
	
	<div class="clearfix"></div>
	
</div> 

<div class="clearfix"></div> 

<div class='container section'>

		<div class="span1">&nbsp;</div>

		<div class="span7 content form-row">
			<h1>Livraison directe bientôt disponible</h1>
			<p>Apprécier notre nourriture n’importe-où. Restez chez-vous, nous allons venir à vous! Disponible au centre-ville uniquement.</p>
			<a href="tel:<?php echo $ctrl->_company_number ?>"><input  type="submit" class="call btn btn-wide " value="Appellez-nous" /></a>
			
		</div>

		<div class="span4">
			<img src="<?php echo $ctrl->getImgUrl('hooters-bag.png') ?>" width="350px" alt="We now deliver"/>			
		</div>

</div>


<?php $ctrl->getBlock('scripts'); ?>

<?php $ctrl->getBlock('footer') ?>

		        		
</body>
</html>