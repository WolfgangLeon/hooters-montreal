<?php 

	/*
	**			 
	**             ENGLISH PAGE BY DEFAULT
	**
	*/
	
	$ctrl = new App();

?>

<?php $ctrl->getBlock('head') ?>

<body>

<div id="wrapper" class="wrapper-flexi">

	<?php $ctrl->getBlock('header') ?>

	<!-- Start Content -->

		<div class="container">
		
            <div class="row"> 
	        	
	        	<div class="span3">&nbsp;</div>
	        	
	        	<div class="span6">
	        	
	        		<div class="inner">
	        		
	        			<div class="form-box">
	        			
	        				<div class="top">

		        				<!--Please complete the following form to ensure your reservation.-->

		        				<div class="inner intro align-center">
					        		<h1>Hooters Montreal</h1>
					        		
					        		<h2>Crescent Street Location Now OPEN</h2>
					        		<p>Our new site is on its way. In the meantime stop on by!</p>		        				
					        	</div> 
		        				
	        				</div>
	        			
	        				<div class="bottom"> 

								<div  id="success" style="display:none" >
								    <span class="green textcenter">
								        <p class="bg-success alert">Thank you for your reservation. Please check your email for a confirmation. Unless there is a verification issue for large group, or we cannot book seating, there is no need for us to call you. If you did not receive a confirmation please check your junk mail folder.</p>
								    </span>
								</div>

								<div id="error" style="display:none" >
								    <span>
								        <p class="bg-alert alert">Something went wrong. Please refresh and try again.</p>
								    </span>
								</div>
																	        				
								<form id="signup" name="signup" onsubmit="return false">

									<input type="hidden" name="lang" id="lang" value="<?php echo $ctrl->_ln ?>">
									
									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Your Name * 
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="text" name="name" id="name" size="30" value="" required="" class="text login_input"  placeholder="Your name">
										</div>
										<div class="clearfix"></div>
									</div>  

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Phone *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="number" name="phone" id="phone" size="30" value="" required="" class="text login_input"  placeholder="Phone Number">
										</div>
										<div class="clearfix"></div>
									</div>
									
									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Email *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="text" name="email" id="email" size="30" value="" required="" class="text login_input"  placeholder="Email Address">
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Guests *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="number" name="guest" id="guest" size="30" value="" required="" class="text login_input"  placeholder="How many people?">
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Occassion *
												</label>
											</div>
										</div>
										<div class="span7">
											<select name="matter" id="matter" required="" class="text login_input">
												<option value="Sporting Event">Sporting Event</option>
												<option value="Private Party">Private Party</option>
												<option value="Dining">Dining</option>
												<option value="Bar">Bar</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Time *
												</label>
											</div>
										</div>
										<div class="span7">
											<select name="time" id="time" required="" class="text login_input">
												<option value="12:00 pm">12:00 pm</option>
												<option value="12:30 pm">12:30 pm</option>
												<option value="1:00 pm">1:00 pm</option>
												<option value="1:30 pm">1:30 pm</option>
												<option value="2:00 pm">2:00 pm</option>
												<option value="2:30 pm">2:30 pm</option>
												<option value="3:00 pm">3:00 pm</option>
												<option value="3:30 pm">3:30 pm</option>
												<option value="4:00 pm">4:00 pm</option>
												<option value="4:30 pm">4:30 pm</option>
												<option value="5:00 pm">5:00 pm</option>
												<option value="5:30 pm">5:30 pm</option>
												<option value="6:00 pm">6:00 pm</option>
												<option value="6:30 pm">6:30 pm</option>
												<option value="7:00 pm">7:00 pm</option>
												<option value="7:30 pm">7:30 pm</option>
												<option value="8:00 pm">8:00 pm</option>
												<option value="8:30 pm">8:30 pm</option>
												<option value="9:00 pm">9:00 pm</option>
												<option value="9:30 pm">9:30 pm</option>
												<option value="10:00 pm">10:00 pm</option>
												<option value="10:30 pm">10:30 pm</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Date *
												</label>
											</div>
										</div>
										<div class="span7">
											<input type="text" name="date" id="date" size="30" value="" autocomplete="off" required="" class="text login_input date calendar"  placeholder="Choose Date">
										</div>
										<div class="clearfix"></div>
									</div>  

									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Location *
												</label>
											</div>
										</div>
										<div class="span7">
											<select name="location" id="location" required="" class="text login_input">
												<option value="1433 Rue Crescent, Montreal">Downtown</option>
												<option value="3320 Taschereau Blvd, Greenfield Park">South Shore</option>
											</select>
										</div>
										<div class="clearfix"></div>
									</div>
									
									<div class="form-row">	
										<div class="span5">
											<div class="inner">
												<label>
													Special Requests
												</label>
											</div>
										</div>
										<div class="span7">
											<textarea name="description" id="description" placeholder="Window seating, level, bar seating, birthday etc."></textarea>
										</div>
										<div class="clearfix"></div>
									</div>  
									    								
									<div class="form-row">					 
										<input id="submit" type="submit" name="submit" value="Make a reservation" class="btn btn-wide btn-extrawide" data-loading-text="Loading...">
										<div class="clearfix"></div>
									</div>   
									
								</form>
								
							</div> 
	        			
	        			</div>
	        			<div class="shadow"></div>
	        			<div class="clearfix"></div>
	        			
	        			
	        		</div>
	        		
	        	</div>
	        	
            </div>  
		
		</div>
	
	<!-- End content -->	
	
	<div class="clearfix"></div>
	
</div> 

<div class="clearfix"></div> 

<div class='container section'>

		<div class="span1">&nbsp;</div>

		<div class="span7 content form-row">
			<h1>Direct Delivery Coming Soon</h1>
			<p>Enjoy our grub anywhere. Stay put, we'll come to you! Available in the downtown area only.</p>
			<a href="tel:<?php echo $ctrl->_company_number ?>"><input  type="submit" class="call btn btn-wide " value="Call us" /></a>
			
		</div>

		<div class="span4">
			<img src="<?php echo $ctrl->getImgUrl('hooters-bag.png') ?>" width="350px" alt="We now deliver"/>			
		</div>

</div>


<?php $ctrl->getBlock('scripts'); ?>

<?php $ctrl->getBlock('footer') ?>

		        		
</body>
</html>