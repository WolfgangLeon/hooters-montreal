<?php 


date_default_timezone_set('America/New_York'); 

Class App 
   {

   		private   $_ln			   = 'en';



   		protected $_company_email  = "reservations@hootersmontreal.com";

   		protected $_company_number = "+15142662222";

   		protected $_fr_title 	   = "Hooters Montreal | Réservations";

   		protected $_en_title 	   = "Hooters Montreal | Reservations";

   		private   $_copyright 	   = "Copyright © Hooters Montreal. All Rights Reserved";

   		private   $_moon_ln		   = "Powered by <strong><a href='http://moonmedia.com' target='_blank'>Moon Media</a></strong>";

   		protected $_hours_title_fr  = 'Heures d’ouvertures';

   		protected $_hours_title_en  = 'Opening Hours';

   		protected $_location_title_fr  = 'Emplacement';

   		protected $_location_title_en  = 'Locations';

   		public 	  $_reservation;

   		public function __construct(){

   			if( isset($_GET['ln']) && !is_null($_GET['ln']) ):

   				$this->_ln = filter_var($_GET['ln'], FILTER_SANITIZE_STRING);

   			endif;

			$this->getLang($this->_ln); // Returns landing page by language automatically. /root/pages/index_en.php by default

   		}

   		public function getLang($ln){
   			
   			$lang = ( isset($ln) ) ? $ln : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

			switch ($lang){
			    case "fr":
			        //echo "PAGE FR";
			        require_once(__ROOT__ . "/pages/index_fr.php");
			        break;
			    case "it":
			        //echo "PAGE IT"; for italian. Maybe in the future?
			        require_once(__ROOT__ . "/pages/index_en.php");
			        break;
			    case "en":
			        //echo "PAGE EN";
			        require_once(__ROOT__ . "/pages/index_en.php");
			        break;        
			    default:
			        //echo "PAGE EN - Setting Default";
			        require_once(__ROOT__ . "/pages/index_en.php");//include EN in all other cases of different lang detection
			        break;
			}

			return $this;
   		}

   		function getBlock($block){

   			if( file_exists( __ROOT__ . '/etc/blocks/' . $block . '.php' ) ):

				require_once( __ROOT__ . '/etc/blocks/' . $block . '.php') ;

			endif;

   			return $this;

   		}

   		function getTemplate($template){

   			$tmp = 'Template Not Found';

   			if( file_exists('./resources/templates/' . $template . '.php') ):

   				$tmp = file_get_contents('./resources/templates/' . $template . '.php');

   			endif;

   			return $tmp;

   		}

   		public function getModel(){
		
			require_once( './etc/connection.php');

			$instance = new connection();
			
			return $instance;	

		}

		public function getLandingTitle(){

			$lang = "_" . $this->_ln . "_title";

			return $this->$lang; 

		}

   		public function getImgUrl($file = null){

   			$_filepath = false;

   			if( file_exists(  './resources/img/' . $file) ):
				$_filepath =  "./resources/img/" . trim($file);
			endif;

			return $_filepath;
		}

		public function getCss($file){

			$_filepath = false;

   			if( file_exists(  './resources/css/' . $file . '.css') ):
				$_filepath =  "./resources/css/" . trim($file) . '.css';
			endif;

			return $_filepath;

		}

		public function getJs($file){

			$_filepath = false;

   			if( file_exists( './resources/js/' . $file . '.js') ):
				$_filepath =  "./resources/js/" . trim($file) . '.js';
			endif;

			return $_filepath;

		}

		public function getDate( $format = 'Y-m-d' ){
			return date($format);
		}

		public function getCopyright(){

			$year = $this->getDate('Y');

			return $this->_copyright . " " . $year . ".<br>" . PHP_EOL . $this->_moon_ln;

		}

		public function sendEmail( $reservation ){

			$reservation['contact'] = $this->_company_email;

			$reservation['subject'] = $reservation['name'] .' - Hooters ' . $reservation['location'] . ' Reservation';

			$reservation['from'] 	= 'Hooters - Montreal';

			return Email::send( $reservation );

		}



   }