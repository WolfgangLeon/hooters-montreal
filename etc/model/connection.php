<?php 
	
require_once('config.php');

Class Connection 

{

	private static $_instance = null;

	private $_conn = null;

	private $_query;

	public function __construct(){

		$host = Config::info('server/host');

		$db   = Config::info('server/db');

		$user = Config::info('server/user');

		$pass = Config::info('server/pass');

		try {

			$this->_conn = new PDO('mysql:host='.$host.';dbname='.$db.';',$user,$pass);

		}catch ( Exception $e ){

			die("Error connecting to database: " . $e->getMessage() . "\n");

		}


	}



	public function connection(){

		if( !isset( self::$_instance ) ):

			self::$_instance = new Connection();

		endif; 

		return self::$_instance;
	}



	public function sql($query , $params = array() ){

		if( $this->_query = $this->_conn->prepare($query) ):

			$cont = 1;

			if( count($params) ):

				foreach( $params as $param):

					$this->_query->bindValue($cont, $param );

					$cont++;

				endforeach;

			endif;

			if( $this->_query->execute() ):		

                self::$_instance = null ;

				$results = $this->_query->fetchAll(PDO::FETCH_ASSOC);

				return $results;

			endif;

		endif;

	}

}