<?php 

$GLOBALS['config'] = array(
			'server' => array(
					'host' 	  => 'localhost', 
					'user' => 'root',
					'pass' => '',
					'db' => 'test'
				)
			
		);

	class Config  {

		public static function info($path = null){
			if ($path):
				$config = $GLOBALS['config'];
				$path = explode('/', $path);


				foreach($path as $keys){
					if( isset($config[$keys]) ):
						$config = $config[$keys];
					else:
						die('Wrong Path');
					endif;
				}

				if( is_array($config) ): die('Wrong Path: Array()'); else: return $config; endif; 
				 

			endif;
	
		}

	}

