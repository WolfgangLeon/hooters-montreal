<?php 
	
include_once( '../init.php');


if( isset($_POST['registration']) ):

	$registration 	= array();

	$rg 			= parse_str($_POST['registration'], $registration);

	$ajax 			= new Ajax($registration);

	$response 		= $ajax->sendEmail( $ajax->_reservation );

	echo json_encode($response);

endif;