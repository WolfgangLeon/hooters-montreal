<?php 
	
	$ctrl = new App();

	$title = $ctrl->getLandingTitle();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $title ?></title>
	<link rel="shortcut icon" href="<?php echo $ctrl->getImgUrl('ico.png') ?>" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta name="description" content="Find our delicious wings, cold beer, sport and definetly Hooters girls. Make your reservation online at Hooters Montreal for free!" />
	<meta name="keywords" content="Hooters menu prices, hooters happy hour, hooters menu prices, hooters locations, hooters gilrs, hooters wings, hooters montreal" /> 
	<link href="<?php echo $ctrl->getCss('bootstrap.min') ?>" rel="stylesheet"/>  
	<link href="<?php echo $ctrl->getCss('style1') ?>" rel="stylesheet"/>
	<link href="<?php echo $ctrl->getCss('font-awesome.min') ?>" rel="stylesheet"/>  
	<link href="<?php echo $ctrl->getCss('jquery-ui.min') ?>" rel="stylesheet"/>
	<link href="<?php echo $ctrl->getCss('updates_v1') ?>" rel="stylesheet"/>
	<link href="<?php echo $ctrl->getCss('responsive') ?>" rel="stylesheet"/> 
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
</head> 

