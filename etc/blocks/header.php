<?php 
	
	$ctrl 			= new App();

	$hours_title 	= ( $ctrl->_ln  == 'en' ) ? $ctrl->_hours_title_en : $ctrl->_hours_title_fr;

	$location_title = ( $ctrl->_ln  == 'en' ) ? $ctrl->_location_title_en : $ctrl->_location_title_fr;

?>
	
<!-- Start Header -->
	<div id="header">
	
		<div class="container">
			<div class="row">
				<div class="span12">
				
					<h1 class="logo"><a href="/" ><img src="<?php echo $ctrl->getImgUrl('logo.png') ?>" alt="Hooters Montreal" width="180px" height="auto" /></a></h1>
					<h2 class="menulink"><a href="#">Menu</a></h2>
					
					<!-- Start Menu -->
					<div id="menu">
						<ul>  
							<li><a id="call-phone" href="tel:<?php echo $ctrl->_company_number ?>"><span class="fa fa-phone-square"></span></a></li>    
							<li><a href="mailto:<?php echo $ctrl->_company_email ?>"><span class="fa fa-envelope"></span> </a></li> 
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> 
									<?php echo $hours_title?>
									<span class="caret"></span> </a>
								<ul class="dropdown-menu">
									<li>Tue 11:00am–3:00am</li>
									<li>Wed	11:00am–3:00am</li>
									<li>Thu 11:00am–3:00am</li>
									<li>Fri	11:00am–3:00am</li>
									<li>Sat	11:00am–3:00am</li>
									<li>Sun	11:00am-3:00am</li>
									<li>Mon	11:00am–3:00am</li>
								</ul>
							</li> 

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" id="drop1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> 
									<?php echo $location_title ?>
									<span class="caret"></span> </a>
								<ul class="dropdown-menu">
									<li>
										<a href="https://www.google.com/maps/place/1433+Rue+Crescent,+Montr%C3%A9al,+QC+H3G,+Canada/@45.497981,-73.5794087,17z/data=!4m5!3m4!1s0x4cc91a41db798bbf:0x76c1d6b13288a8da!8m2!3d45.497981!4d-73.57722" target="_blank" rel="nofollow">
											<span class="fa fa-map-marker"></span>&nbsp;1433 Rue Crescent, Montreal, Quebec <br>
											<a href="tel:+15142662222"><strong>(514) 266 2222</strong></a>
										</a>
									</li>
									<li>
										<a href="https://www.google.com/maps/place/Hooters/@45.4943275,-73.482021,18.57z/data=!4m8!1m2!3m1!2sHooters!3m4!1s0x0:0x5275b50268d00166!8m2!3d45.4939059!4d-73.4812788" target="_blank" rel="nofollow"><span class="fa fa-map-marker">
											
											</span>&nbsp;3320 Taschereau Blvd, Greenfield Park<br>
											<a href="tel:+14505501605"><strong>(450) 550 1605</strong></a>
										</a>
									</li>
								</ul>
							</li> 

							<li><a href="?ln=en"><span>EN</span></a></li>

							<li><a href="?ln=fr"><span>FR</span></a></li>
						</ul> 
					</div> 
					<!-- End Menu -->
					
				</div>
				<div class="clearfix"></div>
			</div>
			
		</div>
	
	</div>
	<!-- End Header -->