<?php 
	$ctrl = new App();
?>


<!--[if lte IE 7]><script src="js/icons-lte-ie7.js"></script><![endif]-->
<script src="<?php echo $ctrl->getJs('jquery.min') ?>"></script>
<script src="<?php echo $ctrl->getJs('jquery.validate.min') ?>"></script>
<script src="<?php echo $ctrl->getJs('bootstrap.min') ?>"></script>
<script src="<?php echo $ctrl->getJs('contact') ?>"></script>
<script src="<?php echo $ctrl->getJs('jquery-ui.min') ?>"></script>
<!--<script src="<?php echo $ctrl->getJs('datepicker-fr') ?>"></script>-->