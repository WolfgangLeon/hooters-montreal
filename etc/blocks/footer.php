<?php 
	
	$ctrl = new App();

?>

<div id="footer">	
	
	<div class="container social">

		<div class="span6">
			<a href="#"><img src="<?php echo $ctrl->getImgUrl('footer-logo.svg') ?>" width="400px" alt="hooters montreal" /></a>
		</div>

		<div class='span6 list'>
			<p> 
				<a href="http://instagram.com/hootersmontreal" target="_blank"><span class="glyph-item mega fa fa-instagram" aria-hidden="true" ></span></a>
				<a href="https://m.facebook.com/HootersMtl/" target="_blank"><span class="glyph-item mega fa fa-facebook-official" aria-hidden="true" ></span></a>
				<a href="https://www.google.com/maps/place/1433+Rue+Crescent,+Montr%C3%A9al,+QC+H3G,+Canada/@45.4979847,-73.579414,17z/data=!3m1!4b1!4m5!3m4!1s0x4cc91a41db798bbf:0x76c1d6b13288a8da!8m2!3d45.497981!4d-73.57722" target="_blank"><span class="glyph-item mega fa fa-map-marker" aria-hidden="true" ></span></a>
				
			</p>
		
		</div>

	</div>
	

	<div class="span12 copyright">

		<p><strong>Contact:</strong><a href="mailto:<?php echo $ctrl->_company_email ?>" > <?php echo $ctrl->_company_email ?></a> | <strong>Careers:</strong><a href="mailto:Careers: hr@hootersmontreal.com" > hr@hootersmontreal.com</a></p>


		<?php echo $ctrl->getCopyright() ?>
	</div>

	<div class="clearfix"></div>

</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96035400-1', 'auto');
  ga('send', 'pageview');

</script>