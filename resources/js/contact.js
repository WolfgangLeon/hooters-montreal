$(document).ready(function(){
    
    var lang  = $('#lang').val()

    var limit = 4

    var hours = $('#time option')

    var min_hour = $('#time option:first-child').val()

    var max_hour = $('#time option:last-child').val()

    var val_error   = {
        'en':{
                name: 'Please enter your name',
                email :'Please enter your email address',
                phone: 'Please enter your phone number',
                date: 'Please choose date',
                location: 'Please select a location',
                guest: 'Please specify number of people',
                early_res: "Your're trying to make a reservation for earlier today, please change your reservation hours",
                limit_res: "Excuse the inconvenience but we cannot take the reservation at this time. Try after "
        },
        'fr':{
                name: 'Veuillez entrer votre nom',
                email :'Veuillez Saisissez votre email',
                phone: 'Veuillez saisir votre numéro de téléphone',
                date: 'Choisissez votre date',
                location: 'Veuillez sélectionner le lieu',
                guest:'Veuillez nous indiquer le nombre de personnes',
                early_res: "Vous essayez de faire une réservation pour plus tôt aujourd'hui, veuillez modifier vos heures de réservation",
                limit_res: "Excusez l'inconvénient mais nous ne pouvons pas prendre la réservation en ce moment. Essayez après"
            }
    }

    function convertReservationTime(date, hour){

        var time = new Date(date);

        var parts = hour.match(/(\d+):(\d+) (am|pm)/);

        if (parts) {
            var hours = parseInt(parts[1]),
                minutes = parseInt(parts[2]),
                tt = parts[3];
            if (tt === 'pm' && hours < 12) hours += 12;

            time.setHours(hours, minutes, 0, 0);
        } 

        return time
        
    }

    function sendReservation(form){

        $.ajax({
            url:"./etc/blocks/reservations.php",
            dataType:"JSON",
            method: "POST",
            data:{
                registration: $(form).serialize()
            },
            success:function(succ){
                $('#signup :input').attr('disabled', 'disabled');
                $('#signup').fadeTo( "slow", 0.15, function() {
                    $(this).find(':input').attr('disabled', 'disabled');
                    $(this).find('label').css('cursor','default');
                    $('#success').fadeIn();
                });
            },
            error:function( error ){
                $('#signup').fadeTo( "slow", 0.15, function() {
                    $('#error').fadeIn();
                });
            }
        })

        return false 
    }

    $(".menulink a").click(function() { 
        $("#menu").stop(true, true).slideToggle('fast');
    });

    $(".hero").stop(true, true).fadeIn('fast');
    $(".form-box").stop(true, true).fadeIn('fast');
    $(".shadow").stop(true, true).fadeIn('fast');

    $('#signup').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },phone: {
                required: true,
                number: true
            },
            date: {
                required: true,
              
            },
            location: {
                required: true
            },
            guest:{
                required:true
            }
            
        },
        messages: {
            name: {
                required: val_error[lang].name,
                minlength: "Your name must consist of at least 2 characters"
            },
            email: {
                required: val_error[lang].email
            },
            phone: {
                required: val_error[lang].phone
            },
            date: {
                required: val_error[lang].date
            },
            location: {
                required: val_error[lang].location
            },
            guest:{
                required:val_error[lang].guest
            }
        },
        errorClass:'error bg-danger',
        submitHandler: function(form) {

            var hour  = $('#time').val()

            var date  = $('#date').val() 

            var time  = convertReservationTime(date, hour)

            var current = new Date()

            var diff = ( time.getTime() - current.getTime() ) / 36e5
           
            if( diff < 0 ){

                alert( val_error[lang].early_res )

            }else if( diff < limit ){

                min_date = new Date()

                min_date.setHours( min_date.getHours() + limit )

                tt  = ( min_date.getHours() < 12 ) ? ' am' : ' pm'

                var hours = ( min_date.getHours() > 12 ) ? min_date.getHours() - 12 : min_date.getHours()

                min_date = hours  + ":" + min_date.getMinutes() + tt

                alert( val_error[lang].limit_res + min_date  )

            }else if( new Date( date ).getDay() == 1 ){
                alert("We apologize the inconvenience but we don't take Mondays reservations.")
            }else{
               
                sendReservation(form)

            }   

        }

    });

    //$('.date.calendar').datepicker( $.datepicker.regional[ lang ] )

    $('.date.calendar').datepicker({
            minDate: 0, 
            maxDate: "+1M",
            beforeShowDay:function(day){
                var day = day.getDay()
                if(day == 1){
                    return [false, 'wolves']
                }else{

                    return [true, 'wolves_good']
                }
            }
        } 

    ) 


})
